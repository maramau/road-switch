# Road Switch

The aim of this project is to simulate the working of a road intersection , more specifically a roundabout or a traffic light crossing, without excluding a possible implementation of a complex network of roads. 
There are a few basic super classes:

| super | Vehicle   | Segment    | Switch              |
| ------| --------- | ---------- | --------------------|
| Sub   | Car       | Node       | Roundabout          |
| Sub   | Truck     | confluence | TafficLightCrossing |
| Sub   | Ambulance | Internals  | Roadoint            |

Other helper classes:
Queue, Generator, Clock.

Queue is the object that keep track "movement" of vehicles on the roads (segment)
Generator for create randomly vehicles to  input in Switch
Clock the timing of the system
The communication between objects uses the concept of Pub-Sub


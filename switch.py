import random, time, math
from turtle import *
import math
# https://pypubsub.readthedocs.io/en/stable/
# pip install pypubsub.
from typing import List
from pubsub import pub
import normalizeTrack as nt
import numpy as np
import pickle

with open('track.list', 'rb') as f:  # Python 3: open(..., 'rb')
    (track_points, px, py) = pickle.load(f)
    f.close()

ACCELERAZIONE = True  # fixme: se False non va tanto bene....
MAX_VELOCITY_LIMIT: int = 5
TYPE = 'roundabout'
NUM_CONFLUENCES: int = 4
NUM_LANES: int = 2
STEP_SECONDS = 1
screen = Screen()  # create the screen
screen.register_shape("car", ((3, -2), (5, 0), (3, 2), (0, 2), (0, -2)))
screen.register_shape("truck", ((15, -3), (20, 0), (15, 3), (0, 3), (0, -3)))
screen.register_shape("ambulance", ((4, -3), (6, 0), (4, 3), (0, 3), (0, -3)))
screen.register_shape("highligth", ((0, -6), (6, -6), (6, 6), (0, 6)))


# screen.register_shape("truck", ((30, -6), (40, 0), (30, 6), (0, 6), (0, -6)))

class Clock:
    tic = 0
    step = 1_000_000_000 * STEP_SECONDS  # step in nanoseconds=s/1_000_000_000 for comodity I choosed 1 second
    State = False
    Active = False

    def start(self):
        # todo study this mechanism, maybe better, but i don't understand it :-(
        self.Active = True
        prev_time = time.time_ns()
        while self.Active:
            actual_time = time.time_ns()
            if actual_time - prev_time > self.step:
                prev_time = actual_time
                self.advance()

    def stop(self):
        self.Active = False

    def set_step(self, step):
        self.step = step

    def get_tic(self):
        return self.tic

    def advance(self):
        self.tic += 1
        pub.sendMessage('clock', toc=self.tic)
        pub.sendMessage('move')
        # print(self.tic)

    def isActive(self):
        return self.Active

    def __sistr__(self):
        pass
        # #print('oggetto di tipo clock')
        # #print('attivo da ', self.tic, ' secondi')

    '''
    Il pattern sarebbe observer ma la libreria pubsub è più efficiente
    for observer in observers:
        observer.Update()
    '''


class Vehicle:
    counter = 0
    velocity = 0  # 14 m/s about equal to 50 km/h
    acceleration = 0  # 10 m/s  formula 1 :-)
    verse = -1  # since I want to manage vehicle in only one direction todo fixme!!s
    # I only have to now in which road (more specifically in which segment)
    # but since, for easier to implement, the vehicle moves toward zero
    # I have to "flip" movement with self.verse
    exit_to = None
    queue = None
    clock = None
    priority = 0  # get priority from the road or set high priority in case of ambulance or police or fire truck
    position = 0.0  # this is the 1D position in the queue
    proximity = 0.0
    road = None
    type = 'vehicle'
    security_distance = 5  # per il momento la gestisco sempre uguale
    color = ''
    max_velocity = MAX_VELOCITY_LIMIT
    is_in_limbo = False
    visibility = 0

    # IMPORTANTE: ricordati di AZZERARE le variabili 
    def __init__(self):
        Vehicle.counter += 1
        self.id = Vehicle.counter
        self.length = 0.0
        self.position = 0

        # only for developing purposes I used turtles as property,
        # if I had to use it I would have implemented vehicle inherited from turtle
        # def class vehicle(turtle):
        self.turtle = Turtle()
        self.turtle.hideturtle()
        self.turtle.penup()
        self.turtle.speed(1000)

    def set_position(self,
                     position: int):  # maybe using numpy bins i could improve precision but with decreasing performances
        assert isinstance(position, int)
        # remember that position is the index of list of track-points
        self.position = position
        if position < 0:  # todo in future maybe that i implement extended queue for overlapping queues
            print('azz, passato liscio!!')
            self.position = 0
        elif position <= self.length:
            self.is_in_limbo = True
        else:
            self.is_in_limbo = False

        # print(pos, self.queue.road_parent.id, self.queue.road_parent.type)
        # print('lung',len(self.queue.road_parent.track))
        # here is tha magic:
        #   instead of compute 2D movement I search the current transformation table value and set the coordinates
        # FIXME!! there is a bug somewhere that make move backward vehicles ....
        # print('position ', position, 'self.p', self.position, 'length', self.queue.length, 'veicolo', self.id,self.type)

        try:
            pp = self.queue.road_parent.track[self.position]
        except:
            print('out of range')
            pp = self.queue.road_parent.track[-1]
        self.turtle.goto(pp)  # todo: differenza tra le due??
        # self.turtle.setpos(self.queue.road_parent.track[pos])
        # self.turtle.setpos(self.get_2D-position(pos)) todo: implementare il metodo corrretto
        # print(self.road.orientation)
        try:
            oo = (self.road.orientation[math.trunc(position)])
        except:
            print('orint fuori range')
            oo = self.road.orientation[-1]
        self.turtle.setheading(oo)
        self.turtle.showturtle()

    def set_visibility(self):
        # for the moment the visibility is just enough for the next step
        self.visibility = self.velocity * self.clock.step / 1_000_000_000 + self.length + self.security_distance

    def get_visibility(self):
        return self.visibility

    def is_at_the_end(self):
        """
        if vehicle is at the end of the queue, that is in the "stop" line
        :return: bool
        """
        # remember that, in this implementation position is in the rear of the vehicle (maybe wrong??)
        if self.position <= self.length:
            return True
        else:
            return False

    def is_first(self):
        """
        if the vehicle is the first of the queue
        :return: bool
        """
        if self.queue.ListOfVehicles[0] == self:
            return True
        else:
            return False

    def can_pass(self, queue):
        """
        if the next queue is free, at least there is enough space to enter
        :param queue:
        :return: bool
        """
        # todo: implement class Node!
        '''
        in this method I check ALL the policies to say YES or NO to the question:
        Can I pass on the next road?
        '''
        n_vehicles = len(queue.ListOfVehicles)
        if n_vehicles > 0:
            last_vehicle: Vehicle = queue.ListOfVehicles[-1]
            if last_vehicle.position < self.queue.length - self.length - self.security_distance:
                return True
            else:
                return False

        else:
            return True

    def has_future(self):
        """
        if there is at least a road attached to this queue

        :return: next Segments (list of), otherwise None
        """
        if len(self.road.Binded_Roads) > 0:
            result = self.road.Binded_Roads
            # print(self.type, self.id, ', in', self.queue.road_parent.type, self.queue.road_parent.id, 'sceglie tra:')

            '''
            for b in result:
                b: Segment
                print('--', b.type, b.id)
            '''
        else:
            result = None

        return result

    def remove_from_queue(self, queue):
        queue.ListOfVehicles.remove(self)
        self.queue = None
        self.road = None

    def put_in_queue(self, queue):
        assert isinstance(queue, Queue)
        assert isinstance(queue.road_parent, Segment)
        # essendo appena entrato non può esssere nel limbo.... ameno che la coda sia cortissima
        self.is_in_limbo = False
        self.queue = queue
        self.road = queue.road_parent
        t = len(self.queue.road_parent.track)
        r = queue.length
        assert t - 1 == r
        # print('track', t, 'queue', r)
        self.set_position(r)  # FIXME + self.length))

    def get_2D_position(self, pos):
        # non so quale sia il meno peggio
        return self.queue.road_parent.track[pos]
        return self.queue.track[pos]

    def transform_position_deprecated(self, p):
        # trasforma la posizione-coda nella carta 2D
        parent: Segment = self.queue.road_parent
        # mi bastano i punti di ingresso ed uscita, perchè per questa definizione la coda è rettilinea
        start = parent.entry_point
        stop = parent.stop_line
        deltax = start[0] - stop[0]
        deltay = start[1] - stop[1]
        # grazie al teorema di Talete calcolo la proporzione nella strada rettilinea
        # definita da start e stop  TODO: implementare il metodo intelligente per strada curvilinea
        # for step in range(self.queue.length):
        # in modo da avere una tabella in cui sono memorizzate le posizioni, non servirà più calcolarli
        # ma verranno richiamati dall'indice= posizione
        ratio = p / self.queue.length
        posx = deltax * ratio
        posy = deltay * ratio
        pos = [stop[0] + posx, stop[1] + posy]

        return pos

    def nextpos(self):
        # due to change "in corso d'opera" I have to re-think the position mechanism
        # in particular the correspondence from position and scale/meters
        time_interval = self.queue.clock.step / 1_000_000_000
        result: int = math.trunc(
            self.position + self.verse * (self.velocity * time_interval))  # (self.acceleration*time_interval**2
        assert isinstance(result, int)
        return result

    def safe_forward(self):
        # goes "forward" one step, only if possible, otherwise Stops!

        self.set_visibility()
        if self.is_forward_free():
            # if toward is free we can move on
            if self.velocity == 0:
                # if we are stopped we have to restart
                self.Start()
                # #self.turtle.color(self.color)
            elif self.velocity < self.max_velocity:
                self.Accelerate()
            # update position because forward is free!
            # vehicle is like a train in a binary: 1D movement!
            # there is  a method that trasform the 1D to 2D
            newpos = self.nextpos()
            if newpos < 0:
                newpos = 0
            elif newpos < self.length:
                newpos = self.length
            self.set_position(newpos)
        else:
            # towards of us there is an obstacle, a vehicle or a stop
            # print('azz! mi fermo!! ', self.id)
            # self.Stop()
            self.Decelerate()
            # time.sleep(1)

            # do not update position, in fact I will stop

    def is_forward_free(self):
        """
        check if behidn the vehicle there is enough space to move
        :return: True if Free False if not
        """
        # returns False if a vehicle ahead is too much near,
        avehicle: Vehicle = self.look_for_vehicle_ahead()
        if avehicle:
            # if there is a vehicle do we collide with it?
            #            delta = self.position - avehicle_next_position - self.length - Vehicle.security_distance
            #
            delta = self.position - avehicle.position - self.length - Vehicle.security_distance
            # todo security distance function of velocuity
            # print('collision possible?????', delta, ' step ', step)

            if delta < self.visibility:
                # Collision possible
                # print('collision possible', delta, ' step ', step)
                # avehicle.turtle.color('yellow')
                return False
            else:
                # collision avoided
                return True
        else:
            # if there is no vehicle have we arrived stop line?
            if self.position < self.length:
                self.position = self.length  # todo check if here changing position is "legal"
                self.is_in_limbo = True  # now officially the vehicle entered limbo zone,
                return False
            return True

    def look_for_vehicle_ahead(self):
        # returns the vehicle ahead if there is one
        # otherwise None (there is no vehicle visible)
        index = self.queue.ListOfVehicles.index(self)
        if index > 0:
            index -= 1
            v = self.queue.ListOfVehicles[index]
            # print('in front of ', self.id, ' there is ', v.id)
            return v
        else:
            return None  # todo check if returning None is "legal" have I to create an empty vehicle to return?

    def Start(self):
        if ACCELERAZIONE == False:
            self.velocity = self.max_velocity
        # self.velocity = MAX_VELOCITY_LIMIT  # todo find a better way to do this, maybe calling accelerate?
        else:
            self.Accelerate()
        # self.turtle.color = self.color

    def Accelerate(self):
        if ACCELERAZIONE == False:
            self.Start()
        else:
            self.velocity += 3  # todo implement accelerate dependent by clock

        assert isinstance(self.turtle, Turtle)
        # self.turtle.color('white')
        ##self.turtle.color('white')

    def Decelerate(self):
        if ACCELERAZIONE == False:
            self.Stop()
        else:
            self.velocity -= 3  # todo implement decelerate dependent by clock

        # self.turtle.color('grey')

    def Stop(self):
        if ACCELERAZIONE == False:
            self.velocity = 0
        # self.velocity = MAX_VELOCITY_LIMIT  # todo find a better way to do this, maybe calling decelerate?
        else:
            self.Decelerate()

        # self.turtle.color('black')

    def Set_priority(self):
        # self.priority=self.road.priority    # todo implement priority concept
        self.priority = 5


class Queue:
    counter = 0
    id = 0
    clock = None
    # a structure in which we insert from top new vehicles and
    # fetch from the bottom in present
    road_parent = None
    length: int = 0  # rectified length of track, fixme there is an error but i think this is
    # "ininfluent" for statistical purposes

    ListOfVehicles: [Vehicle] = list()
    isReady = True
    binded_segment = None
    binded_queue = None
    binded_Nodes = list()
    limbo_occupied = 0

    # IMPORTANTE: ricordati di AZZERARE le variabili
    def __init__(self, aClock: Clock):
        self.clock = aClock
        Queue.counter += 1
        self.id = Queue.counter
        self.ListOfVehicles: [Vehicle] = list()
        pub.subscribe(self.advance_vehicles, 'move')
        self.road_parent = None
        self.isReady = True
        self.limbo_occupied = 0
        self.binded_Nodes = list()

    def set_road(self, road):
        self.road_parent = road

    def attach_Node(self, aNode):
        assert isinstance(aNode, Node)
        self.binded_Nodes.append(aNode)

    def auto_set(self):
        # when we put a queue in a road this queue is able to adjust its parameter
        # according to the road, for the moment only with rect roads! fixme
        # self.road_parent = road
        p: Segment = self.road_parent
        '''
        if self.road_parent.length > 0:
            self.length = self.road_parent.length
        else:
            st = p.start
            end = p.stop
            self.length = math.sqrt((end[0] - st[0]) ** 2 + (end[1] - st[1]) ** 2)
        '''
        self.length = len(p.track) - 1  # fixme sovrascrive il valore all'interno dell'if

    def Feed(self, vehicle: Vehicle):
        # rimasugli di esperimenti precedenti
        if vehicle is not None:

            # lego a doppio filo la coda col veicolo: la prima sa che veicoli ha
            # i secondi sanno in che coda stanno!
            vehicle.put_in_queue(self)
            self.ListOfVehicles.append(vehicle)
            # "metto" il veicolo all'inizio della coda, cioè posizione è la lunghezza della coda
            # vehicle.set_position(self.length) moved inside put_in_queue

            # TODO: "chiudere" l'ingresso temporaneamente, davvero???
            # self.isReady = False  # until all the vehicle has entered the queue this is busy
        else:
            print('sorry, no vehicle')

    def Fetch(self):

        # returning None if there is no vehicle is the right way? FIXME
        vehicle = None
        if len(self.ListOfVehicles) > 0:
            vehicle = self.ListOfVehicles.pop(0)
        return vehicle

    def destroy(self, vehicle):
        vehicle.turtle.goto(10000, 100000)
        del vehicle.turtle
        vehicle.turtle = None
        # vehicle.turtle: Turtle.hideturtle()

        self.ListOfVehicles.remove(vehicle)
        del vehicle

    def is_ready_for(self, vehicle):
        if len(self.ListOfVehicles) > 0:
            if self.ListOfVehicles[-1].position < vehicle.position - vehicle.length - vehicle.security_distance:
                return True
            else:
                return False
        else:
            return True

    def look_sx_dx(self, queue, where='sx'):
        w = {'sx': 0, 'dx': -1}
        if len(queue.ListOfVehicles) > 0:
            my: Vehicle = self.ListOfVehicles[0]
            opposite: Vehicle = queue.ListOfVehicles[w[where]]

            if opposite.position < my.position - my.length - my.security_distance:
                return False
            else:
                return True
        else:
            return True

    def is_dx_free(self, queue):
        if len(queue.ListOfVehicles) > 0:
            v_rear: Vehicle = self.ListOfVehicles[0]
            v_dx: Vehicle = queue.ListOfVehicles[-1]

            if v_dx.position < v_rear.position - v_rear.length - v_rear.security_distance:
                return True
            else:
                return False
        else:
            return True

    def is_sx_free(self, queue):
        if len(queue.ListOfVehicles) > 0:
            v_rear: Vehicle = self.ListOfVehicles[0]
            v_sx: Vehicle = queue.ListOfVehicles[0]

            if v_sx.position < v_rear.position - v_rear.length - v_rear.security_distance:
                return True
            else:
                return False
        else:
            return True

    def is_next_queue_ready(self, queue):
        if self.is_sx_free(queue):
            return False
        if len(queue.ListOfVehicles) > 0:
            v_rear: Vehicle = self.ListOfVehicles[0]
            v_ahead: Vehicle = queue.ListOfVehicles[-1]

            if v_ahead.position < v_rear.position - v_rear.length - v_rear.security_distance:
                return True
            else:
                return False
        else:
            return True

    def move_first_on_next_queue(self, queue):
        """
        when the first vehicle completed the passage from this queue
        remove the vehicle from the list and feed it into queue passed
        :param queue:
        :return: nothing
        """
        first_vehicle: Vehicle = self.ListOfVehicles[0]
        self.limbo_occupied = 0  # reset length of limbo occupied
        first_vehicle.is_in_limbo = False  # now the vehicle is not in the limbo,
        queue.Feed(first_vehicle)  # entered the new queue
        self.ListOfVehicles.remove(first_vehicle)

    def are_there_vehicles(self):
        if len(self.ListOfVehicles) > 0:
            return True
        else:
            return False

    def advance_vehicles(self):
        if self.are_there_vehicles():
            for vehicle in self.ListOfVehicles:
                # print('v', vehicle.id, vehicle.type, vehicle.road.id, vehicle.road.type)
                if vehicle.is_first():
                    if vehicle.is_at_the_end():
                        future_roads = vehicle.has_future()
                        # vehicle.turtle.color = 'magenta'
                        vehicle.turtle: Turtle.color('magenta')
                        if future_roads:
                            # todo: i have to check my policy
                            # quindi: se la policy di questa coda me lo consente
                            #           vado oltre
                            print("evvai verso l'infinito e oltre!!!!!!")
                            # vehicle.turtle.color = 'brown'
                            vehicle.turtle.color('brown')
                            vehicle.turtle.shape('highligth')
                            for r in future_roads:
                                r.highlight_track('yellow')
                            time.sleep(0.3)
                            # probabilmente codificata come lista di code, quando passo da una all'altra
                            # rimuovo la coda abbandonata e proseguo, la meta è raggiunta quando la lista è vuota

                            future_road: Segment = random.choice(future_roads)
                            future_road.highlight_track('magenta')
                            time.sleep(0.3)
                            for r in future_roads:
                                r: Segment
                                r.switch_off_track()
                            # qui sta l'inghippo: chi decide dove andare?

                            future_queue = random.choice(future_road.Queues)
                            future_queue = vehicle.exit_to.Queues[0]
                            #vehicle.exit_to = future_queue  # fixme  qui scelgo a caso di fronte al bivio! NOOO
                            print('scelto: ', future_queue.road_parent.type, future_queue.road_parent.id)
                            vehicle.turtle.shape(vehicle.type.lower())
                            # fixme: i have to delegate to node the decision, or maybe, take decision

                            # looking at ALL the nodes that are binded to this queue
                            if self.binded_Nodes[0].moving_policy(vehicle):
                                # if vehicle.can_pass(future_queue):
                                vehicle.turtle.color('cyan')
                                self.move_first_on_next_queue(future_queue)
                            else:
                                # alt, stop-line say no trespassing
                                vehicle.Stop()
                        else:
                            # arrived at the edge of simulation, in reality the vehicle will go forward but we
                            # DESTROY ITTTT!!!!!!!!!!!!!
                            self.destroy(vehicle)
                    else:
                        # Vehicle is "far" from stop-line, go ahead avoiding collision
                        vehicle.safe_forward()
                else:
                    # vehicle isn't the first, go ahead avoiding collision
                    vehicle.safe_forward()
        else:
            # no vehicles in the queue, nothing to do
            pass

    def set_is_ready____(self):
        if self.ListOfVehicles[-1].position >= self.length - 10:
            self.isReady = False
        else:
            self.isReady = True


class Segment:
    counter = 0
    id = 0
    Priority = 0
    Queues: [Queue] = list()
    Input_roads = list()
    Binded_Roads = list()
    Switch_parent = None
    isInputFree = False
    isOutputFree = False
    direction = ''
    type = ''  # 'in' 'out'
    track = list()  # [[x0,y0],[x1,y1]....[xN,yN]]      n=len(track)-1  !!!!!!!!!!!!
    orientation = 0  # [[o1],[o2].............[oN]]      n=len(track)-1  !!!!!!!!!!!!
    entry_point = 0  # [xN,yN]
    stop_line = 0  # [x0,y0]
    length = 0  # float is the arc-length of the track
    color = ''  # debug purposes only

    def __init__(self):
        self.color = ''

    def Set_parent(self, aSwitch):
        self.Switch_parent = aSwitch

    def highlight_track(self, color):
        tt = Turtle()
        tt.hideturtle()
        tt.pencolor(color)
        tt.penup()
        tt.goto(self.entry_point)
        tt.pendown()
        tt.goto(self.stop_line)

    def switch_off_track(self):
        self.highlight_track(self.color)

    def Feed(self, vehicle):
        # cerco la PRIMA corsia libera più a destra
        # e quindi inserisco il veicolo
        # todo: VERIFY use this instead of directly queue.feed
        for q in self.Queues:
            if q.is_ready_for(vehicle):
                q.Feed(vehicle)
                break

    def Set_track(self, list_of_points):
        assert isinstance(list_of_points, list)
        assert isinstance(list_of_points[0], list)
        self.orientation = list()
        # fixme! pay attention, the vehicle enter the queue from the last index!!!
        # first point is the "stop" line
        # last point is the entry point
        how_many = len(list_of_points)
        if how_many == 2:
            # we have only start and stop points, in this order, but the track is in inverse order
            # have I done a big bullshit?? fixme
            self.entry_point = list_of_points[-1]
            self.stop_line = list_of_points[0]
            # length=math.sqrt()
            # todo: meglio spostare la normalizzazione in un metodo separato
            l = np.linalg.norm(np.subtract(np.array(self.entry_point), np.array(self.stop_line)))
            length = int(np.trunc(l))  # ensure not overflow
            self.length = length
            track = np.linspace(self.stop_line, self.entry_point,
                                self.length)  # todo: understand if bettr with or without self
            self.track = np.ndarray.tolist(track)
            # assert isinstance(self.track, list)
            # let's compute orientation
            deltax = self.stop_line[0] - self.entry_point[0]
            deltay = self.stop_line[1] - self.entry_point[1]
            orientation = math.atan2(
                deltay,
                deltax) * 180 / math.pi + 90  # fixme why -90?  Capit!! dipende da come è orientata la mia shape! ruotata di 90 gradi rispetto all'orientazione sua
            #            self.track = np.linspace(self.start, self.stop, length)  # evenly spaced (i think more or less 1) points
            self.orientation = [orientation for _ in self.track]  # ensure that same length as track
            # self.orientation.append(orientation)
            # self.Queues[0]: Queue.auto_set()
            print('strada: ', self.type, self.id, '\n',
                  '  start:', self.entry_point, '\n',
                  '  stop', self.stop_line)
        elif how_many > 3:
            # already a track list point but we have to normalize that
            # start and stop are still the same!
            self.entry_point = list_of_points[-1]
            self.stop_line = list_of_points[0]
            # fixme!! in this moment I pass anormalized track so I disabled the function
            if True:
                pass
            else:
                (track_best, track_length, track_orient) = nt.normalize_track(list_of_points)

            self.track = list_of_points
            # fixme non calcola bene la lunghezza, dovrei usare l'approsimazione polinomiale usata per la normalizazione
            # todo far restituire alla funzione la lista e la lunghezza
            summmarize = 0
            for i, ii in zip(list_of_points[:-1], list_of_points[1:]):
                summmarize += math.sqrt((i[0] - ii[0]) ** 2 + (i[1] - ii[1]) ** 2)
            self.length = summmarize
            # let's calculate orientation:
            # delta is how many points forward I look for compute
            delta = 2
            ori = list()
            for before, after in zip(list_of_points[:-delta], list_of_points[delta:]):
                deltax = after[0] - before[0]
                deltay = after[1] - before[1]
                ori.append(math.atan2(deltay, deltax) * 180 / math.pi - 90)  # fixme why -90??
            for _ in range(delta):
                ori.append(ori[-1])
            self.orientation = ori
            assert len(self.orientation) == len(self.track)
            assert len(self.track) == len(list_of_points)
            # self.Queues[0]: Queue.auto_set()
        else:
            print("inserisci almeno due coordinate")

    def Fetch(self):
        pass

    def policy(self):
        pass

    def Bind(self, Road):
        self.Binded_Roads.append(Road)
        Road.Attach_Input_Road()

    def Attach_Input_Road(self, Road):
        self.Input_roads.append(Road)

    def Attach_Input_Roads(self, list_of_input_roads):
        self.Input_roads.extend(list_of_input_roads)

    def Attach_Queue_old(self, queue):

        # the method is here due to make possible to add lanes to a road in any moment
        # (have you ever seen men at work :-)?)
        self.Queues.append(queue)
        queue.road_parent = self


class Switch:
    clock = None
    ListOfOutputRoads: [Segment] = list()
    ListOfInputRoads: [Segment] = list()
    List_of_internals: [Segment] = list()

    # IMPORTANTE: ricordati di AZZERARE le variabili
    def __init__(self, aClock):
        self.clock = aClock

    def Bind_Output(self, listOfOutputRoads: [Segment]):
        self.ListOfOutputRoads.extend(listOfOutputRoads)
        for r in listOfOutputRoads:
            r: Segment.Bind(Switch)
            r: Segment.Attach_Input_Roads()

    def Attach_Input(self, listOfInputRoads: [Segment]):
        self.ListOfInputRoads.extend(listOfInputRoads)

    def build(self):
        pass


class Node:
    bindedIn = list()
    bindedOut = list()
    Switch_parent = None

    def __init__(self):
        self.bindedOut = list()
        self.Switch_parent = None
        self.bindedIn = list()

        pass

    def Bind(self, aQueue: Queue, type='in'):  # todo maybe I have to make bindIn and bindOut?
        """
        type in ['in','out']
        it will bind queues in the node, to specify the order of precedence
        :param aQueue: the queue that "enter" the node
        :param type:
        :return:
        """
        if type == 'in':
            self.bindedIn.append(aQueue)
        else:
            self.bindedOut.append(aQueue)

        # now we tell to the queue that it has to obey this node!
        aQueue.attach_Node(self)

    def moving_policy(self, aVehicle: Vehicle):
        # così, tanto per
        return True


class precedence_simple(Node):
    """
    the class that set priority to lanes, and tell if a vehicle can pass or not to next queue
    """

    # IMPORTANTE: ricordati di AZZERARE le variabili di classe
    def __init__(self, aQueueIn: Queue, aQueueOut: Queue):
        super().__init__()
        self.Bind(aQueueIn)
        self.Bind(aQueueOut, 'out')

    def moving_policy(self, aVehicle: Vehicle):

        """

        :return: True if can pass False otherwise
        """
        assert len(self.bindedIn) == 1
        assert len(self.bindedOut) == 1
        # if next queue has enough space then go
        # let's look forward
        num_vehicles_ahead = len(self.bindedOut[0].ListOfVehicles)
        # are there vehicles in front of us?
        if num_vehicles_ahead > 0:
            # yes! let's keep the last one
            v_last: Vehicle = self.bindedOut[0].ListOfVehicles[-1]
            assert isinstance(v_last, Vehicle)
            needed_space = aVehicle.get_visibility()
            # is enough far?
            if v_last.position < needed_space:  # remember that, due to design, position is decreasing towards stop-line
                # yes, return True, you can pass away
                return True
            else:
                # sorry no, too much near you have to stop!
                return False
        else:
            # no vehicles go straight on!
            return True


class precedence_sx(Node):
    # IMPORTANTE: ricordati di AZZERARE le variabili di classe
    def __init__(self, aQueueInSx: Queue, aQueueInDx: Queue, aQueueOut: Queue):
        super().__init__()
        self.Bind(aQueueInSx)  # so, by design, Sx is index 0
        self.Bind(aQueueInDx)  # same reason    Dx in index 1
        self.Bind(aQueueOut, 'out')

    def moving_policy(self, aVehicle: Vehicle):
        # in which input road is vehicle?
        if aVehicle.queue == self.bindedIn[0]:
            # the vehicle is in the Sx road: has precedence
            # the idea is that, in this moment, we fall in
            # precedence_simple and i would call it....
            num_vehicles_ahead = len(self.bindedOut[0].ListOfVehicles)
            # are there vehicles in front of us?
            if num_vehicles_ahead > 0:
                # yes! let's keep the last one
                v_last: Vehicle = self.bindedOut[0].ListOfVehicles[-1]
                assert isinstance(v_last, Vehicle)
                needed_space = aVehicle.get_visibility()
                # is enough far?
                if v_last.position < needed_space:  # remember that, due to design, position is decreasing towards stop-line
                    # yes, return True, you can pass away
                    return True
                else:
                    # sorry no, too much near you have to stop!
                    return False
            else:
                return True
        else:
            # the vehicle is in the DX road: must suffer!!
            # se la corsia non è libera
            num_vehicles_sx = len(self.bindedIn[0].ListOfVehicles)
            # are there vehicles in sx?
            if num_vehicles_sx > 0:
                # yes! let's keep the first one
                v_first: Vehicle = self.bindedIn[0].ListOfVehicles[0]
                assert isinstance(v_first, Vehicle)
                needed_space = v_first.get_visibility()
                # is too much near?
                if v_first.position > needed_space:  # remember that, due to design, position is decreasing towards stop-line
                    # yes, return True, you can pass away
                    return True
                else:
                    # sorry no, too much near you have to stop!
                    return False
            else:
                # no vehicles: go!!
                return True


class precedence_sx2(Node):
    # IMPORTANTE: ricordati di AZZERARE le variabili di classe
    def __init__(self, aQueueInSx: Queue, aQueueInDx: Queue, aQueueOutSx: Queue, aQueueOutDx: Queue):
        super().__init__()
        self.Bind(aQueueInSx)  # so, by design, Sx is index 0
        self.Bind(aQueueInDx)  # same reason    Dx in index 1
        self.Bind(aQueueOutSx, 'out')  # so, by design, Sx is index 0
        self.Bind(aQueueOutDx, 'out')  # same reason    Dx in index 1

    def moving_policy(self, aVehicle: Vehicle):
        """

        :param aVehicle: the vehicle that is being simulated now
        :return:
        """
        # in which input road is vehicle?
        # needed for decide who has precedence
        print('moving policy')
        if aVehicle.queue == self.bindedIn[0]:
            # the vehicle is in the Sx road: has precedence
            # the idea is that, in this moment, we fall in
            # precedence_simple and I would call it....

            print('dove passo? ',
                  [['coda', out.id, 'strada', out.road_parent.type, out.road_parent.id] for out in self.bindedOut])
            # look at which exit wants vehicle
            exit_to = aVehicle.exit_to
            # remember that, by design, index 1 is confluence, 0 internal
            if exit_to == self.bindedOut[1]:
                # we have to exit this road
                road_out = self.bindedOut[1]
            else:
                # we round in, don't exit!
                road_out = self.bindedOut[0]

            num_vehicles_ahead = len(road_out.ListOfVehicles)
            # are there vehicles in front of us?
            if num_vehicles_ahead > 0:
                # yes! let's keep the last one
                v_last: Vehicle = self.bindedOut[0].ListOfVehicles[-1]
                assert isinstance(v_last, Vehicle)
                needed_space = aVehicle.get_visibility()
                # is enough far?
                if v_last.position < needed_space:  # remember that, due to design, position is decreasing towards stop-line
                    # yes, return True, you can pass away
                    return True
                else:
                    # sorry no, too much near you have to stop!
                    return False
            else:
                return True
        else:
            # the vehicle is in the DX road: must suffer!!
            # if lane is not free:
            num_vehicles_sx = len(self.bindedIn[0].ListOfVehicles)
            # are there vehicles in sx?
            if num_vehicles_sx > 0:
                # yes! let's keep the first one
                v_first: Vehicle = self.bindedIn[0].ListOfVehicles[0]
                assert isinstance(v_first, Vehicle)
                needed_space = v_first.get_visibility()
                # is too much near?
                if v_first.position > needed_space:  # remember that, due to design, position is decreasing towards stop-line
                    # yes, return True, you can pass away
                    return True
                else:
                    # sorry no, too much near you have to stop!
                    return False
            else:
                # no vehicles: go!!
                return True


class precedence_dx(Node):
    # IMPORTANTE: ricordati di AZZERARE le variabili di classe
    def __init__(self, aQueueIn: Queue, aQueueOut: Queue):
        super().__init__()
        self.Bind(aQueueIn)
        self.Bind(aQueueOut, 'out')

    def moving_policy(self):
        pass


class Switch_Factory:
    # type in [ roundabout,trafficlightcrossing,roadjoint...]
    # let's HAND CREATE roundabout TODO: implement correctly segment factory
    # let's HAND CREATE roundabout TODO: implement correctly position of segments
    def Create2(self, type, list_of_main_tracks, aclock):
        # la lista è composta da liste di percorsi/start-stop
        pass

    def Create(self, type, list_of_lanes_direction, tracks, aclock):  # ,segmentFactory):
        # list_of_lanes_direction is a list of lists: every nested list is
        # the list of number of lanes in the confluence, 'in' and 'out', in this order:
        # for example:[[2,2],[2,None],[None,4],[None,1],['in':1],[3,3]]
        # means that there are 6 confluences, 2 double-sided and four one-way: two in input and two in output
        # with for each the number of lanes
        # numConfluence = len(list_of_lanes_direction)
        segments_in: [Segment] = list()
        segments_out: [Segment] = list()
        # let's create  confluences
        # creo le strade "esterne" che costruiranno la rotonda
        # delle liste del numero di corsie e dei punti partenza e arrivo
        # fixme!!! this is wrong! dirs and tracks have no equal dimensions!!!!!
        # for now i build the lists so that it works just fine but....

        # per ogni traccia indicata da costruire..
        for dirs, track in zip(list_of_lanes_direction, tracks):
            # costruisco
            # la strada in ingresso...
            if dirs[0] != 0:  # solo se c'è :-)
                road_in = Segment_Factory.Create('confluence',
                                                 aClock=aclock, numLane=dirs[0], direction='in', track=track[0])
            # o in uscita... (dipende dalla colonna di list of line direction
            if dirs[1] != 0:  # solo se c'è :-)
                road_out = Segment_Factory.Create('confluence',
                                                  aClock=aclock, numLane=dirs[1], direction='out', track=track[1])

            # e le appendo a liste apposite
            segments_in.append(road_in)
            segments_out.append(road_out)

        # needed in the step below
        # segments_out_rearranged = segments_out[1:]
        # segments_out_rearranged.append(segments_out[0])
        # : solo per test, attacco una entrata con l'uscita attigua, solo per testare il meccanismo di fetching
        # for seg_in, seg_out in zip(segments_in, segments_out_rearranged):
        #   seg_in.Bind(seg_out)
        # print('IN ', seg_in.id, ' binded to: ', seg_in.Binded_Roads[0].id, ' resto ', seg_in.Binded_Roads)

        if type == 'roundabout':
            # here is the magic, we create the generic roudabout and...
            obj = Roundabout(aclock, segments_in, segments_out, 1)
        elif type == 'trafficlightcrossing':
            # TBD todo implement trafficlight crossing creation
            pass
        elif type == 'roadjoint':
            # TBD todo implement road joint creation
            pass
        return obj

    def Bind(self, OutputRoad, next_road):
        # todo implement the binding with other roads or Switches
        pass


class Segment_Factory:
    input_roads = None
    output_roads = None
    queueLanes: [Queue] = list()

    @staticmethod
    def Create(type, aClock, numLane, direction, track):
        """

        :param type: 'confluence o 'internal'
        :param aClock:
        :param numLane: int
        :param direction:  'in' o 'out'
        :param track: [[startx,starty],[stopx,stopy]] or list of points
        :return:
        """
        # segment = Segment
        if type == 'internal':
            segment = Internal(aClock)

        elif type == 'confluence':
            segment = Confluence(aClock, direction)

        elif type == 'lane':  # maybe I will do better substitute lane with queue
            segment = Lane(aClock)

        # segment.
        segment.Set_track(track)

        segment.direction = direction

        # now we have to create and attach lanes/queues
        for q in range(0, numLane):
            # print('corsia',q)
            qi = Queue(aClock)
            segment.Attach_Queue(qi)
            qi.auto_set()
            # print('attacco coda ',qi,' a segmento' , segment)

        return segment

    def Attach_input(self, listOfInputRoads):
        self.input_roads: [Segment] = listOfInputRoads

        pass

    def Bind_output(self, listOfOutputRoads):
        self.output_roads: [Segment] = listOfOutputRoads
        pass


class Generator:
    counter = 0
    id = 0
    queue: Queue = None  # fixme obsolete??
    road: Segment = None

    '''
    @classmethod
    def num_istanze(cls):
        #print(cls.counter)
    '''

    def Bind_road(self, road: Segment):
        assert isinstance(road, Segment)
        self.road = road
        # self.queue = road.Queues[0]

    # IMPORTANTE: ricordati di AZZERARE le variabili 
    def __init__(self, aClock):
        Generator.counter += 1
        self.id = Generator.counter
        self.queue = None
        self.road = None
        # ------------ register listener ------------------
        # arguments are: the name of the function choosed before, without parenthesis, and name of topic, sendere related
        # pub.subscribe(self.listener1, 'rootTopic')
        # pub.subscribe(self.is_there_a_vehicle, 'vehicle')
        # con questo mi sono sottoscritto al colpo di clock, cioè non serve che ciclo su tutti gli oggetti,
        # faranno da soli

        pub.subscribe(self.vehicle_create, 'clock')
        self.clock = aClock

    def pick_one(self, list_choices, prob_choices, percent=True):
        """
        a very basic method to random choice a type of vehicle
        :param list_choices: a list of things to choose
        :param prob_choices: respectively percentual (0-100)
        :param percent: a parameter that say if prob is percentuial or normalized
        :return: an element of list_choices
        """

        rand = (random.random()) * 100 ** percent  # I really don't like if statement !
        options = np.array(list_choices)
        bins = np.cumsum(prob_choices)
        assert bins[-1] == 100
        which = np.digitize(rand, bins)
        choosed = options[which]
        return choosed

    def vehicle_create(self, toc):
        """
        for each of the queues in the road
            it creates a random vehicle and, if there is  enough space,
            it feeds the road with this vehicle
        :return: vehicle or None
        """
        veicoli_disponibili = ['c', 't', 'a']
        prob_veicoli = [80, 15, 5]
        # todo implement the distribution in time
        strade_disponibili = self.road.Switch_parent.ListOfOutputRoads
        uscite_disponibili = [r.Queues[0] for r in strade_disponibili]
        prob_uscite = [50, 35, 15, 0]
        creator = {'c': Car(self.clock), 't': Truck(self.clock), 'a': Ambulance(self.clock)}
        exist = random.randrange(100)
        if exist > 80:  # probabilità del 80 % di non avere veicoli
            vehicle = creator[self.pick_one(veicoli_disponibili, prob_veicoli)]

            vehicle.exit_to = self.pick_one(uscite_disponibili, prob_uscite)
            for qu in self.road.Queues:
                if qu.is_ready_for(vehicle):
                    qu.Feed(vehicle)
                    return vehicle

                else:
                    # fixme: is this the correct way? (due to the distribution )
                    del vehicle
                    return None

    def vehicle_create2(self, toc):
        """
        for each of the queues in the road
            it creates a random vehicle and, if there is  enough space,
            it feeds the road with this vehicle
        :return: vehicle or None
        """
        veicoli_disponibili = ['c', 't', 'a']
        prob_veicoli = [80, 15, 5]

        exist = random.randint(1, 10)
        if exist > 8:
            vv = random.randint(100)  # FIXME TODO sistemare il generatore
            vehicle = self.is_there_a_vehicle(vv)
            for qu in self.road.Queues:
                if qu.is_ready_for(vehicle):
                    qu.Feed(vehicle)
                    return vehicle

                else:
                    # fixme: is this the correct way? (due to the distribution )
                    del vehicle
                    return None

    def is_there_a_vehicle(self, v):
        """
        a very basic method to random choice a type of vehicle
        :param v: a number in a range
        :return: a vehicle
        """
        if v is None:
            # print('no vehicle yet')
            vehicle = None
        elif v < 6:
            # print('a car')
            vehicle = Car(self.clock)
        elif v < 8:
            # print('a truck')
            vehicle = Truck(self.clock)
        elif v < 9:
            # print('an ambulance')
            vehicle = Ambulance(self.clock)
        else:
            # print('unknown vehicle')
            vehicle = Vehicle
        return vehicle

    def Attach_Clock(self, aClock):
        self.clock = aClock


class Roundabout(Switch):
    # IMPORTANTE: ricordati di AZZERARE le variabili di classe
    def __init__(self, aClock, list_of_conf_in, list_of_conf_out, num_lanes_internal):
        super().__init__(aClock)
        self.ListOfInputRoads = list_of_conf_in
        self.ListOfOutputRoads = list_of_conf_out
        self.List_of_internals = self.create_internals(num_lanes_internal)
        self.set_precedences()
        self.set_parent_to_children()

    def set_parent_to_children(self):
        for r_in, r_out, r_int in zip(self.ListOfInputRoads, self.ListOfOutputRoads, self.List_of_internals):
            r_in.Switch_parent = self
            r_out.Switch_parent = self
            r_int.Switch_parent = self

    def create_internals(self, num_lanes=1):
        assert isinstance(num_lanes, int)
        # todo: looking forward... with this mechanism we are not able to stick real road over the roundabout...
        # maybe I would have a method that do not CREATE but STICK internal in the roundabout
        # now is the turn of internals, being a one-way road that keeps to the exit the direction in 'out' by default
        internals: [Segment] = list()
        # fixme at the moment i don't consider one way road
        # r_in:Confluence
        # r_out:Confluence
        # for binding purposes I rearrange the list to better couple roads
        out_rearranged = self.ListOfOutputRoads[1:]
        out_rearranged.append(self.ListOfOutputRoads[0])

        for r_in, r_out in zip(self.ListOfInputRoads, out_rearranged):
            # print('in:', r_in.id, 'out', r_out.id)
            # print('strada in ingresso:', r_in.type,r_in.id)
            # print('  entry-point', r_in.entry_point)
            # print('  stop-line  ', r_in.stop_line)
            assert isinstance(r_in, Segment)
            # remember that the first point is the stop-line!
            i_track = [r_out.entry_point, r_in.stop_line]
            assert isinstance(i_track, list)
            assert isinstance(i_track[0], list)
            internal = Segment_Factory.Create('internal', aClock=self.clock, numLane=num_lanes, direction='out',
                                              track=i_track)
            internal.Switch_parent = self

            internals.append(internal)

        nn = len(self.ListOfInputRoads)
        # let's pipe
        for here in range(nn):
            # here we are binding the internal x with the subsequent internal x+1  and
            # attaching the precedent internal x-1 so to have a circular path
            next = (here + 1) % nn
            prev = (here - 1) % nn
            internals[here].Bind(internals[next])

            # and now we have to attach the confluences at the internals
            r_in: Segment = self.ListOfInputRoads[here]
            r_out = self.ListOfOutputRoads[here]
            i_here = internals[prev]
            i_next = internals[here]
            # print(prev, here, next)
            # print(r_in.type, r_in.id)
            # print(r_out.type, r_out.id)
            # print(i_here.type, i_here.id)
            # print(i_next.type, i_next.id)
            i_here.Bind(r_out)  # this is the exit from roundabout
            r_in.Bind(i_next)  # this, otherwise, is the entrance
        # exit()
        '''
        for i in self.ListOfInputRoads:

            #print('input della rotonda',i.id, i.type)
            for b in i.Binded_Roads:
                #print('---', b.id, b.type)
        '''
        # exit()
        assert isinstance(internals, list)
        assert isinstance(internal, Internal)
        assert isinstance(internals[0], Internal)
        '''
        for i in internals:
            i: Internal
            print('interno ',i.type, i.id, )
            for b in i.Binded_Roads:
                print('   binded: ',b.id,b.type)
            for inp in i.Input_roads:
                print(' attached: ',inp.id,inp.type)
        '''
        return internals

    def bind_roads(self):
        pass

    def set_precedences(self):
        """
        here we are! we have to tell to every "node-in" which precedence to choose:
        obviously, being a roundabout, we pick precedence_sx!
        :return:
        """
        for intern in self.List_of_internals:
            # if we look at the internals we are sure to match every node.
            # for simplicity
            # remember that, due to design,
            # In binded_roads index 0 is for internal,
            # In Input_roads   index 0 is for Confluence
            # index 1 for confluence
            # FIXME!! quick and dirty!
            q_sx: Queue = intern.Queues[0]  # who is IN the roundabout have priority, internal
            if isinstance(intern.Input_roads[0], Confluence):
                q_dx = intern.Input_roads[0]  # who wants to enter has to wait, confluence
            else:
                q_dx = intern.Input_roads[1]  # who wants to enter has to wait, confluence
            q_dx = q_dx.Queues[0]  #
            if isinstance(intern.Binded_Roads[0], Internal):
                q_out_round = intern.Binded_Roads[0].Queues[0]  # keep turning, internal
            else:
                q_out_round = intern.Binded_Roads[1].Queues[0]  # keep turning, internal
            if isinstance(intern.Binded_Roads[1], Confluence):
                q_exit = intern.Binded_Roads[1].Queues[0]  # exit! confluence
            else:
                q_exit = intern.Binded_Roads[0].Queues[0]  # exit! confluence

            assert isinstance(q_sx.road_parent, Internal)
            assert isinstance(q_dx.road_parent, Confluence)
            assert isinstance(q_out_round.road_parent, Internal)
            assert isinstance(q_exit.road_parent, Confluence)

            round_node = precedence_sx2(q_sx, q_dx, q_out_round, q_exit)
            q_out: Queue.attach_Node(round_node)
            q_dx: Queue.attach_Node(round_node)
            q_sx: Queue.attach_Node(round_node)

        # per ogni strada in ingresso devo creare un nodo con precedenza a sx e le code corrispomdenti


class TrafficLightCrossing(Switch):
    pass


class RoadJoint(Switch):
    pass


class Internal(Segment):
    counter = 0
    id = 0

    # IMPORTANTE: ricordati di AZZERARE le variabili
    def __init__(self, aClock):
        super().__init__()
        self.clock = aClock
        self.type = 'internal'
        self.parent = None

        # adirection is in ['in','out']
        self.Binded_Roads: [Segment] = list()
        self.Queues: [Queue] = list()
        Internal.counter += 1
        self.id = Internal.counter
        self.orientation = 0

        self.Input_roads = list()

        self.Switch_parent = None
        self.isInputFree = False
        self.isOutputFree = False
        self.direction = 'out'
        self.track = list()  # [[x0,y0],[x1,y1]....[xN,yN]]      n=len(track)-1  !!!!!!!!!!!!
        self.orientation = 0  # [[o1],[o2].............[oN]]      n=len(track)-1  !!!!!!!!!!!!
        self.entry_point = 0  # [xN,yN]
        self.stop_line = 0  # [x0,y0]
        self.length = 0  # float is the arc-length of the track

    def Attach_Queue(self, queue):
        """
        being a road we have to attach a queue to manage vehicles!
        :param queue:
        :return:
        """
        self.Queues.append(queue)
        queue.road_parent = self

    def Bind(self, Road):
        self.Binded_Roads.append(Road)
        Road.Attach_Input_Road(self)


class Confluence(Segment):
    counter = 0
    id = 0

    # IMPORTANTE: ricordati di AZZERARE le variabili 
    def __init__(self, aClock, adirection, ):
        # adirection is in ['in','out']
        super().__init__()
        self.clock = aClock
        self.type = 'confluence'
        self.direction = adirection
        self.Binded_Roads: [Segment] = list()
        self.Queues: [Queue] = list()
        Confluence.counter += 1
        self.id = Confluence.counter
        self.orientation = 0

    def Bind(self, Road):
        self.Binded_Roads.append(Road)
        Road.Attach_Input_Road(self)

    def Attach_Queue(self, queue):
        # attacco questa coda lla strada e...
        self.Queues.append(queue)
        queue.set_road(self)


# is this useless?  is it the same concept of Queue? Maybe yes!
class Lane(Segment):
    # IMPORTANTE: ricordati di AZZERARE le variabili 
    def __init__(self, aClock):
        super().__init__()
        #        assert isinstance(aQueue, Queue)

        self.clock = aClock
        self.type = 'lane'


class Car(Vehicle):
    # IMPORTANTE: ricordati di AZZERARE le variabili 
    def __init__(self, aClock):
        super().__init__()
        self.clock = aClock
        self.length = 5
        self.max_velocity = MAX_VELOCITY_LIMIT
        self.velocity = self.max_velocity  # m/s, equal about 50 km/h
        self.exit_to = None
        self.priority = 0  # get priority from the road or set high priority in case of ambulance or police or fire truck
        self.position = 100
        self.proximity = 0.0
        self.road = None
        self.type = 'CAR'
        self.color = 'red'
        self.turtle.shape('car')
        # self.turtle.color(self.color)
        # self.turtle.penup()
        is_in_limbo = False


class Truck(Vehicle):
    # IMPORTANTE: ricordati di AZZERARE le variabili 
    def __init__(self, aClock):
        super().__init__()
        self.clock = aClock
        self.length = 20
        self.max_velocity = MAX_VELOCITY_LIMIT * 0.8
        self.velocity = self.max_velocity
        self.type = 'TRUCK'
        self.position = 200
        self.color = 'blue'
        self.turtle.shape('truck')
        # self.turtle.color(self.color)
        is_in_limbo = False


class Ambulance(Vehicle):
    # IMPORTANTE: ricordati di AZZERARE le variabili 
    def __init__(self, aClock):
        super().__init__()
        self.clock = aClock
        self.length = 6
        self.max_velocity = 2 * MAX_VELOCITY_LIMIT
        self.velocity = self.max_velocity
        self.priority = 10
        self.type = 'AMBULANCE'
        self.position = 300
        # self.turtle=Turtle()
        self.color = 'green'
        self.turtle.shape('ambulance')
        # self.turtle.color(self.color)
        self.turtle.penup()
        is_in_limbo = False


def main():
    # print('simulatore di traffico stradale su un incrocio a scelta')
    # print('nella fattispecie una rotonda')
    # with all pieces builded we have to put them together in the right crossing:
    the_clock = Clock()
    # input roads number    1        2      3       4
    # the rule is:
    # list of roads:
    #   each roads is the list of:
    #       queues in, queues out, each queue is:
    #           list of start and stop point
    list_numbers_lanes = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]
    off = 4
    ext = 200
    inter = 50
    meta = len(track_points) // 2
    rIN = track_points[:meta]
    rOUT = track_points[meta:]
    # [[rIN], [rOUT]],
    # ---------------IN----------    -------------OUT----------
    # -  stop-line     entry_point   stop-line       entry_point
    list_tracks = [

        [[[inter, off], [ext, off]], [[ext, -off], [inter, -off]]],
        [[[-off, inter], [-off, ext]], [[off, ext], [off, inter]]],
        [[[-inter, -off], [-ext, -off]], [[-ext, off], [-inter, off]]],
        [[[off, -inter], [off, -ext]], [[-off, -ext], [-off, -inter]]]

    ]
    # [
    #               [[ext, off], [inter, off]], [[inter, -off], [ext, -off]]
    #               ],
    crossing = Switch_Factory().Create(type=TYPE, list_of_lanes_direction=list_numbers_lanes, aclock=the_clock,
                                       tracks=list_tracks)

    # print(crossing)
    generators: List[Generator] = list()
    queues: List[Queue] = list()
    print('rotonda', crossing.ListOfInputRoads)
    c = Car(the_clock)
    t = c.turtle
    t.penup()
    # road=Confluence()
    l = list()

    def segnaletica(t, road, color):
        t.color(color)
        t.setpos(road.entry_point)
        # t.write('start')
        t.write(road.id)
        road.color = color
        t.pendown()
        t.goto(road.stop_line)
        t.penup()

    for road in crossing.ListOfInputRoads:
        segnaletica(t, road, 'red')
    for road in crossing.ListOfOutputRoads:
        segnaletica(t, road, 'green')
    for road in crossing.List_of_internals:
        segnaletica(t, road, 'blue')

    for confluence_in in crossing.ListOfInputRoads:
        # print('IN', confluence_in)
        # print(confluence_in.start)
        # creo tanti generatori di veicoli quante sono le corsie in entrata
        # nel caso in esame mi aspetto che siano 4 (una corsia per strada)
        # creo un generatore per starda in ingresso, non uno per corsia, sarà lui a mettere
        # i veicoli nella corsia libera più a destra
        if confluence_in != 0 or confluence_in is not None:
            # if road_in == confluence_in:
            g = Generator(the_clock)
            g.Bind_road(confluence_in)
            # print('corsia', lane_in)
            generators.append(g)
    '''           
    s=Screen()
    t=Turtle
    t.penup()
    t.goto(-300,q.length)
    t.pendown()
    t.goto(300, q.length)
    '''

    # loop principale
    while the_clock.tic < 100:
        the_clock.advance()
        # print('----', crossing.clock.get_tic())
        # time.sleep(1)


if __name__ == '__main__':
    start_time = time.time()
    main()
    end_time = time.time()
    duration = end_time - start_time
    print("runtime for this program {}".format(duration))

import numpy as np

"""
based on:
https://stackoverflow.com/questions/18244305/how-to-redistribute-points-evenly-over-a-curve
risposta 4

"""


def normalize_track(track,Num_point=0):
    """
    process the points in input-track to return Num_point equally distanced points
    in the same curve

    :param track: a list of point, the input track
    :param Num_point: desidered number point in the same track, 0 means AUTO
    :return: a new list of Num_point, the same track divided equally
    """
    # how many points will be uniformly interpolated?
    #nt = steps.size

    number_in = len(track)          # number of points on the curve
    track_array = np.array(track)   # from list to array, due to optimization
    p_start = track_array[0, :]     # point of entrance
    p_stop = track_array[-1, :]     # point of stop line
    # calculate the distance between first and last point
    start_end_distance = np.linalg.norm(np.subtract(p_start, p_stop))

    # Compute the chordal arclength of each segment.
    chord_len = (np.sum(np.diff(track_array, axis=0) ** 2, axis=1)) ** (1 / 2)
    arc_len=np.sum(chord_len)
    # Normalize the arclengths to a unit total
    chordlen_normalized = chord_len / arc_len
    # cumulative arclength, for each point the curvilineal length from the start
    cum_arc_len = np.append(0, np.cumsum(chordlen_normalized))

    if Num_point==0:
        Num_point=int(round(arc_len/10))
        print(Num_point)

    # point equally spaced in arclength
    steps = np.transpose(np.linspace(0, 1, Num_point))  # an array Nx1


    # N, remember, is the array of equally spaced point from 0 to 1
    tbins = np.digitize(steps, cum_arc_len)  # bin index in which each N is in
    # numpy.where(condition[, x, y])
    # Return elements chosen from x or y depending on condition.
    # catch any problems at the ends
    minor=np.where(tbins <= 0 | (steps <= 0))
    major=np.where(tbins >= number_in | (steps >= 1))
    tbins[minor] = 1
    tbins[major] = number_in - 1

    s = np.divide((steps - cum_arc_len[tbins]), chord_len[tbins - 1])
    new_points = track_array[tbins, :] + np.multiply((track_array[tbins, :] - track_array[tbins - 1, :]), (np.vstack([s] * 2)).T)

    return new_points

